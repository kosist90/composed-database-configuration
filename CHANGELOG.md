# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.2] - 2019-04-25
### Changed
- updated dependencies and recompiled project

## [0.1.1] - 2019-04-23
### Changed
- fixed an issue with dependency paths causing edit time project conflicts

## [0.1.0] - 2019-04-22
### Added
- initial release 



// ## [Unreleased]
// ### Added
// ### Changed
// ### Deprecated
// ### Removed
// ### Fixed
// ### Security